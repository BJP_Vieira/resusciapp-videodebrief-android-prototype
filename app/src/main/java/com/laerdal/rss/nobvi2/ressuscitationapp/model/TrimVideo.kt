package com.laerdal.rss.nobvi2.ressuscitationapp.model

import android.graphics.Bitmap
import android.provider.MediaStore
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import java.sql.Timestamp

open class TrimVideo:RealmObject(){

    var tag: Tag? = null
    var path:String=""
    @LinkingObjects("trimmedVideos")
    val owners: RealmResults<Video>? = null
}