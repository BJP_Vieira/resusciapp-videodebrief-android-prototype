package com.laerdal.rss.nobvi2.ressuscitationapp.bus

class RxEvent {
    data class NotifyViewWithMessage(val message: String)
    data class EventPublishCompressionData(val dataList: Map<Double, Double>)
    data class NotifyViewWithCompressiondata(val data: Double)


}