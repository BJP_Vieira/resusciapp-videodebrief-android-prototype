package com.laerdal.rss.nobvi2.ressuscitationapp.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Video() : RealmObject(){
    @PrimaryKey  var id: Long = 0
    var path: String = ""
    var trimmedVideos: RealmList<TrimVideo> = RealmList()
    var tags: RealmList<Tag> = RealmList()

}