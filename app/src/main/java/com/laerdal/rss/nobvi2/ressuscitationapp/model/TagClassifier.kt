package com.laerdal.rss.nobvi2.ressuscitationapp.model

import io.realm.RealmObject

enum class TagClassifier (val value:Int) {
    Neutral(-1),
    Positive(1),
    Negative(0)
}