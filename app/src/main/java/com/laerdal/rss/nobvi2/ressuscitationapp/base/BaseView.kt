package com.laerdal.rss.nobvi2.ressuscitationapp.base

import android.app.Activity
import android.content.Context
import com.polidea.rxandroidble2.RxBleClient

/**
 * Base view any view must implement.
 */
interface BaseView {
    /**
     * Returns the Context in which the application is running.
     * @return the Context in which the application is running
     */
    fun getContext(): Context

}