package com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth

import android.content.Context
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.internal.RxBleLog


import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BLEServiceModule

