package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector


@Subcomponent(modules = [DebriefingModule::class])
interface DebriefingComponent : AndroidInjector<DebriefingActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<DebriefingActivity>()
}


