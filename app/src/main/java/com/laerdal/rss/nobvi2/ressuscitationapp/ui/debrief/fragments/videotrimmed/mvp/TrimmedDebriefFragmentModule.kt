package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.TrimmedDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp.TagDebriefContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp.TagDebriefPresenter
import dagger.Module
import dagger.Provides

@Module
class TrimmedDebriefFragmentModule {

    @Provides
    internal fun provideDetailFragmentView(graphDebriefFragment: TrimmedDebriefFragment): TrimmedDebriefContract.View {
        return graphDebriefFragment
    }


}