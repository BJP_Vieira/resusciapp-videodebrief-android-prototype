package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.ForwardTimeShiftFromTagTime
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.ReverseTimeShiftFromTagTime
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.concurrent.ConcurrentLinkedQueue

val currentTime: Long
    get() {
        return System.currentTimeMillis()
    }

fun retrieveVideoDuration(context: Context, videoFile: File): Long {
    val retriever = MediaMetadataRetriever()
    retriever.setDataSource(context, Uri.fromFile(videoFile))
    val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
    retriever.release()
    return time.toLong()

}


private fun buildMetadataWithTag(tagTimeList: ConcurrentLinkedQueue<Long>): String {
    val sb = StringBuilder()
    sb.append(";")
    sb.append("FFMETADATA1\n\n")

    for (tagTime in tagTimeList) {
        val start = tagTime - ReverseTimeShiftFromTagTime
        val end = tagTime + ForwardTimeShiftFromTagTime

        sb.append("[CHAPTER]\n")
        sb.append("TIMEBASE= 1/1000\n")
        sb.append("START= $start\n")
        sb.append("END= $end\n")
        sb.append("title=$tagTime\n\n")
    }

    return sb.toString()
}

fun generateMetadata(tagTimeList: ConcurrentLinkedQueue<Long>, filePath: String) {
 val metadataInfo= buildMetadataWithTag(tagTimeList)

    try {
        val txtFile = File(filePath)
        val writer = FileWriter(txtFile)
        writer.append(metadataInfo)
        writer.flush()
        writer.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }

}


