package com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth

import dagger.Subcomponent
import dagger.android.AndroidInjector


@Subcomponent(modules = [BLEServiceModule::class])
interface BleServiceComponent : AndroidInjector<BLEService> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<BLEService>()
}


