package com.laerdal.rss.nobvi2.ressuscitationapp

import com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth.BLEService
import com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth.BLEServiceModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilder {

    @ContributesAndroidInjector(modules = [BLEServiceModule::class])
    internal abstract fun contributeBleService(): BLEService

}