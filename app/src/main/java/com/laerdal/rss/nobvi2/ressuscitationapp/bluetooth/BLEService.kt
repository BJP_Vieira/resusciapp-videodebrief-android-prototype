package com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth

import android.app.Service
import android.bluetooth.BluetoothGattService
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.jakewharton.rx.ReplayingShare
import com.laerdal.rss.nobvi2.ressuscitationapp.BuildConfig
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxBus
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxEvent
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils.currentTime
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.*
import com.polidea.rxandroidble2.NotificationSetupMode

import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import com.polidea.rxandroidble2.helpers.ValueInterpreter
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import dagger.android.AndroidInjection
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.zipWith
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class BLEService : Service() {

    @Inject
    lateinit var rxBleClient: RxBleClient

    private lateinit var scanDisposable: Disposable
    private lateinit var conncetionDisposable: Disposable

    private lateinit var scanObserver: Observable<ScanResult>
    private lateinit var connectionObserver: Observable<RxBleConnection>
    private lateinit var bleDevice: RxBleDevice

    val dataArrayList: HashMap<Double, Double> = HashMap()

    val data: LinkedList<Double> = LinkedList()
    val time: ArrayList<Double> = ArrayList()

    private var startCollect: Boolean = false
    private var tempTime: Long = 0L

    private val disconnectTriggerSubject = PublishSubject.create<Boolean>()
    private val binder = LocalBinder()

    private var videoIterator = 0
    private var scanSettings: ScanSettings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
            .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
            .build()

    private var scanFilter: ScanFilter = ScanFilter.Builder()
            .setManufacturerData(LaerdalManufacturerId, byteArrayOf(DeviceType.ResusciAnne.device.toByte()))
            .build()


    inner class LocalBinder : Binder() {
        val service: BLEService
            get() = this@BLEService
    }


    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
        scanObserver = rxBleClient.scanBleDevices(scanSettings, scanFilter).subscribeOn(AndroidSchedulers.mainThread()).unsubscribeOn(AndroidSchedulers.mainThread())

    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            val action = intent.action
            if (action != null) {
                when (action) {
                    StartScan -> {
                        initiateScanningForLaerdalDevices()
                    }
                    StartCollectingData -> {
                        startCollect = true
                        tempTime = currentTime
                        dataArrayList.clear()
                        data.clear()
                        time.clear()
                        videoIterator=0
                    }
                    StopCollectingData -> {
                        startCollect = false
                    }
                    RetrieveData -> {
                        startCollect = false
                        val map: Map<Double, Double> =
                                time.zip(data)
                                        .toMap()

                        RxBus.publish(RxEvent.EventPublishCompressionData(map))

                    }
                    Disconnect -> {
                        startCollect = false
                        disconnectTriggerSubject.onNext(true)
                    }
                }
            }
        }
        return Service.START_REDELIVER_INTENT
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        return false
    }

    override fun onDestroy() {
        disconnectTriggerSubject.onNext(true)
        super.onDestroy()
    }


    private fun initiateScanningForLaerdalDevices() {
        scanDisposable = scanObserver
                .doOnNext { scanResult ->
                    scanResult.bleDevice
                    println(scanResult.bleDevice.name + " " + scanResult.rssi)
                    if (scanResult.rssi < 0 && scanResult.rssi > -200) {
                        RxBus.publish(RxEvent.NotifyViewWithMessage("Trying to connect to your closest Resusci Anne"))

                        println(scanResult.bleDevice.name + " with " + scanResult.rssi + " is now trying to connect...")
                        establishConnectionWithDevice(scanResult.bleDevice)
                        scanDisposable.dispose()
                    }
                }.doOnError { t: Throwable? ->
                    println(t?.message)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }


    private fun establishConnectionWithDevice(device: RxBleDevice) {
        bleDevice = device
        connectionObserver = device
                .establishConnection(false)
                .takeUntil(disconnectTriggerSubject)
                .compose(ReplayingShare.instance())



        conncetionDisposable = connectionObserver.flatMap { rxBleConnection ->
            Observable.merge(
                    rxBleConnection.writeCharacteristic(AuthenticateCharacteristicGuid, AuthenticationKey).toObservable(),
                    rxBleConnection.readCharacteristic(AuthenticationStatusCharacteristicGuid).toObservable()).takeLast(1)
        }
                .doOnNext {
                    if (it[0].compareTo(AuthenticationStatus.Success.status) == 0) {
                        RxBus.publish(RxEvent.NotifyViewWithMessage("Device is Now Authenticated, attempt to initiate Session..."))
                        println("device is Now Authenticated, attempt to initiate Session...")
                        startSession()
                        conncetionDisposable.dispose()
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { throwable -> println(throwable.message) }
                .subscribe({ _ -> }, { t -> t.stackTrace })


    }


    private fun startSession() {
        val setIndication = connectionObserver
                .flatMap { rxBleConnection ->
                    rxBleConnection.setupIndication(SessionControl, NotificationSetupMode.COMPAT)
                    rxBleConnection.writeCharacteristic(SessionControl, byteArrayOf(SessionControlCommand.Start.sessionValue, CompressionsOnly.Compressions.value, CompressionsOnly.Ventilations.value)).toObservable().zipWith(
                            rxBleConnection.writeCharacteristic(ModeSession, byteArrayOf(SessionType.RaceMode.value)).toObservable())
                }
                .doOnError { t: Throwable? -> println(t?.message) }
                .doOnNext {
                    RxBus.publish(RxEvent.NotifyViewWithMessage("Resuci Anne is now ready to broadcast to this mobile device"))
                    println("Connection and Indication were successful set... for device,  ${bleDevice.name}")
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ _ -> setBleNotification() }, { t -> t.stackTrace })
    }

    private fun setBleNotification() {
        val compressionServiceNotification = connectionObserver
                .flatMap { rxBleConnection ->
                    rxBleConnection.discoverServices().flatMap { services -> services.getService(Compression).map(BluetoothGattService::getCharacteristics) }
                            .flatMapObservable { s -> Observable.fromIterable(s) }
                            .flatMap { characteristic ->
                                rxBleConnection.setupNotification(characteristic)
                                        .flatMap { notificationObservable -> notificationObservable.flatMap { b -> Observable.just(b).zipWith(Observable.fromArray(getBytesFromUUID(characteristic.uuid))) } }
                            }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ notification -> onNotificationReceived(notification) }, { t -> t.stackTrace })
    }


    private fun onNotificationReceived(bytes: Pair<ByteArray, ByteArray>) {
        val notificationByte = bytes.first
        val characteristicUUI = getUUIDFromBytes(bytes.second)
        if (characteristicUUI == CompressionWave) {
            val notificationValue = ValueInterpreter.getIntValue(notificationByte, ValueInterpreter.FORMAT_UINT8, 4).toDouble()


            if (startCollect) {
                val videoRefTime = (currentTime - tempTime) / 1000
                data.add(notificationValue)
                time.add(videoIterator++.toDouble())
                RxBus.publish(RxEvent.NotifyViewWithCompressiondata(notificationValue))
                println(notificationValue)
            }
        }
    }

    companion object {
        private const val PREFIX = BuildConfig.APPLICATION_ID + ".BleService:"
        const val StartScan = PREFIX + "START_SCAN"
        const val StartCollectingData = PREFIX + "COLLECT_DATA"
        const val StopCollectingData = PREFIX + "STOP_COLLECT_DATA"
        const val RetrieveData = PREFIX + "RETRIEVE_DATA"
        const val Disconnect = PREFIX + "DISCONNECT"
    }
}
