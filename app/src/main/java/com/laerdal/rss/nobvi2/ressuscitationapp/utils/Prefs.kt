package com.laerdal.rss.nobvi2.ressuscitationapp.utils

import android.content.Context
import android.content.SharedPreferences
import com.laerdal.rss.nobvi2.ressuscitationapp.App

class Prefs(context: Context) {

    private val preferencesName = " com.laerdal.rss.nobvi2.ressuscitationapp:Prefs"
    private val fileSource = "FilePath"
    private val prefs: SharedPreferences = context.getSharedPreferences(preferencesName, 0)

    var originalFilePath: String?
        get() = prefs.getString(fileSource, null)
        set(value) = prefs.edit().putString(fileSource, value).apply()


    fun clearPrefs() {
        prefs.edit().remove(fileSource).apply()
    }

}