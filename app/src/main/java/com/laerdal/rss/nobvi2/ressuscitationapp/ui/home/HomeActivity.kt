package com.laerdal.rss.nobvi2.ressuscitationapp.ui.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.laerdal.rss.nobvi2.ressuscitationapp.R
import com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth.BLEService
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.mvp.HomeActivityContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.mvp.HomeActivityPresenter
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.PERMISSIONS
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.Prefs
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : DaggerAppCompatActivity(), HomeActivityContract.View {

    @Inject
    lateinit var presenter: HomeActivityPresenter
    @Inject
    lateinit var prefs: Prefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        bt_session.setOnClickListener {
            presenter.goToSessionView()
            prefs.clearPrefs()
        }
        bt_connect.setOnClickListener { presenter.connectToLaerdalBLEDevice() }

        if (!hasPermissionsGranted(PERMISSIONS))
            askForPermissions()

    }


    override fun showMessageDialog(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        presenter.unSubscribeEventListener()
        stopService(Intent(this, BLEService::class.java))
        super.onDestroy()
    }

    private fun askForPermissions() {
        val rationale = "Please provide access to Camera, Audio, Location and Storage in order to record sessions..."
        val options = Permissions.Options()
                .setRationaleDialogTitle("Info")
                .setSettingsDialogTitle("Warning")

        Permissions.check(this, PERMISSIONS, rationale, options, object : PermissionHandler() {
            override fun onGranted() {
            }

            override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {


            }
        })
    }

    private fun hasPermissionsGranted(permissions: Array<String>) =
            permissions.none {
                ContextCompat.checkSelfPermission((this as Activity), it) != PackageManager.PERMISSION_GRANTED
            }
}
