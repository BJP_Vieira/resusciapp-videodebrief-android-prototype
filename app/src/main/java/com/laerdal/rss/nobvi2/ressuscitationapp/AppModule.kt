package com.laerdal.rss.nobvi2.ressuscitationapp

import android.app.Application
import android.content.Context
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.Prefs
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.internal.RxBleLog
import dagger.Binds

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import io.realm.Realm

/**
 * Created by braulio.vieira on 14/10/18.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun bindApplication(app: App): Application {
        return app
    }

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }


    @Provides
    @Singleton
    internal fun provideBleClient(context: Context): RxBleClient {
        RxBleClient.setLogLevel(RxBleLog.DEBUG)
        return RxBleClient.create(context)
    }

    @Provides
    @Singleton
    internal fun providePrefs(context: Context): Prefs {
        return Prefs(context)
    }

    @Provides
    @Singleton
    internal fun provideFFmpeg(context: Context): FFmpeg {
        return FFmpeg.getInstance(context)
    }


    @Provides
    @Singleton
    internal fun provideRealm(): Realm {
        return Realm.getDefaultInstance()
    }


}
