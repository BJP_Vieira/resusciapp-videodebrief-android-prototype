package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import android.widget.Toast
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries

import com.laerdal.rss.nobvi2.ressuscitationapp.R
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Tag
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.customviews.VideoControllerView
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp.TagDebriefContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp.TagDebriefPresenter
import dagger.android.support.DaggerFragment
import io.realm.RealmList
import kotlinx.android.synthetic.main.fragment_tagged_video_debrief.*
import java.io.File
import java.io.IOException
import java.util.ArrayList
import javax.inject.Inject

class TagDebriefFragment : DaggerFragment(), TagDebriefContract.View, SurfaceHolder.Callback, View.OnTouchListener, MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControl {

    @Inject
    lateinit var presenter: TagDebriefPresenter

    private lateinit var player: MediaPlayer
    private lateinit var controller: VideoControllerView
    private var videoSource: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            videoSource = it.getString(DebriefingActivity.videoSource)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_tagged_video_debrief, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.setOnTouchListener(this)

        videoSurface.holder.addCallback(this)

        player = MediaPlayer()
        controller = VideoControllerView(context)

        try {
            if (context != null) {
                player.setDataSource(context!!, Uri.fromFile(File(videoSource)))
                player.setOnPreparedListener(this)
            }
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: SecurityException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        videoSource?.let { presenter.prepareTagArray(it) }
    }

    override fun onDestroy() {
        player.stop()
        super.onDestroy()
    }

    override fun setTagsIntoVideoControllerView(tagArray: RealmList<Tag>?, videoDuration: Int) {
        controller.setTags(tagArray,videoDuration)
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        controller.show()
        return false
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        player.setDisplay(holder)
        player.prepareAsync()
    }

    override fun onPrepared(mp: MediaPlayer?) {
        controller.setMediaPlayer(this)
        controller.setAnchorView(videoSurfaceContainer as FrameLayout)
    }

    override fun showErrorMessage(errorMessage: String) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()

    }

    override fun start() {
        player.start()
    }

    override fun pause() {
    }

    override fun getDuration(): Int {
        return player.duration
    }

    override fun getCurrentPosition(): Int {
        return player.currentPosition
    }

    override fun seekTo(pos: Int) {
        player.seekTo(pos)
    }

    override fun isPlaying(): Boolean {
        return player.isPlaying
    }

    override fun getBufferPercentage(): Int {
        return 0
    }

    override fun canPause(): Boolean {
        return true
    }

    override fun canSeekBackward(): Boolean {
        return true
    }

    override fun canSeekForward(): Boolean {
        return true
    }

    override fun isFullScreen(): Boolean {
        return false
    }

    override fun toggleFullScreen() {
    }


    companion object {
        @JvmStatic
        fun newInstance(videoSource: String) =
                TagDebriefFragment().apply {
                    arguments = Bundle().apply {
                        putString(DebriefingActivity.videoSource, videoSource)
                    }
                }
    }
}
