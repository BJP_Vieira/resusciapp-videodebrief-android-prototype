@file:JvmName("Constants")

package com.laerdal.rss.nobvi2.ressuscitationapp.utils

import android.Manifest
import android.util.TimeUtils
import java.util.concurrent.TimeUnit


val PERMISSIONS = arrayOf(Manifest.permission.RECORD_AUDIO,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION)

const val ReverseTimeShiftFromTagTime = 3 * 1000
const val ForwardTimeShiftFromTagTime = 5 * 1000
