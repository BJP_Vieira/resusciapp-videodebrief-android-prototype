package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries

import com.laerdal.rss.nobvi2.ressuscitationapp.R
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.mvp.GraphDebriefContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.mvp.GraphDebriefPresenter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_graph_debrief.*
import javax.inject.Inject
import com.jjoe64.graphview.GridLabelRenderer
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Tag
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import io.realm.RealmList
import kotlin.collections.HashMap


class GraphDebriefFragment : DaggerFragment(), GraphDebriefContract.View {

    @Inject
    lateinit var presenter: GraphDebriefPresenter

    lateinit var compressionGraphSeries: LineGraphSeries<DataPoint>
    private var videoSource: String? = null
    private var videoPosition = 0
    private var seriesSize=0

    private val seekBarHandler = Handler()
    private val updateSeekbarRunnable = object : Runnable {
        override fun run() {
            seekBar?.progress = videoView.currentPosition
            seekBarHandler.postDelayed(this, 50)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            videoSource = it.getString(DebriefingActivity.videoSource)
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_graph_debrief, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        compressionGraphSeries = LineGraphSeries()
        compressionGraphSeries.color = resources.getColor(R.color.color_yellow)
        compressionGraphSeries.thickness = 3


        compression_wave_graph.viewport.isXAxisBoundsManual = true

        compression_wave_graph.viewport.isScalable = true
        compression_wave_graph.viewport.setScalableY(true)

        compression_wave_graph.viewport.setDrawBorder(false)
        compression_wave_graph.setBackgroundColor(resources.getColor(R.color.color_black))

        compression_wave_graph.gridLabelRenderer.gridStyle = GridLabelRenderer.GridStyle.NONE
        compression_wave_graph.gridLabelRenderer.isHorizontalLabelsVisible = false
        compression_wave_graph.gridLabelRenderer.isVerticalLabelsVisible = false


        compressionGraphSeries.setOnDataPointTapListener { _, dataPoint ->
            val seek=(dataPoint.x.toInt()*videoView.duration)/seriesSize
            videoView.seekTo(seek)
            setPause()
        }

        videoView.setVideoPath(videoSource)

        videoView.setOnCompletionListener {
            bt_play.setBackgroundResource(R.drawable.ic_play)
            seekBarHandler.removeCallbacks(updateSeekbarRunnable)

        }

        bt_play.setOnClickListener {
            onPlayPause()
        }

        seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser)
                    videoView.seekTo(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }


        })

    }

    override fun onResume() {
        super.onResume()
        presenter.setTags(videoSource)
        presenter.getCompressionDataFromTheBleService()
    }


    override fun onDestroy() {
        presenter.unsubscribeFromEvents()
        super.onDestroy()
    }

    private fun onPlayPause() {
        if (videoView.isPlaying) {
            setPause()
        } else {
            seekBarHandler.postDelayed(updateSeekbarRunnable, 0)
            setPlay()
        }
    }

    private fun setPlay() {
        bt_play.setBackgroundResource(R.drawable.ic_pause)
        if (videoPosition > 0) {
            videoView.seekTo(videoPosition)
            videoPosition = 0
        }
        videoView.start()
    }

    private fun setPause() {
        bt_play.setBackgroundResource(R.drawable.ic_play)

        videoView.pause()
        videoPosition = videoView.currentPosition
    }

    override fun setTagsIntoVideoControllerView(tags: RealmList<Tag>?, videoDuration: Int) {
        seekBar?.setTags(tags as MutableList<Tag>, videoDuration)
    }

    override fun sendCompressionDataToView(data: Map<Double, Double>) {
        seriesSize=data.size

        compression_wave_graph.viewport.setMinX(0.0)
        compression_wave_graph.viewport.setMinY(0.0)
        compression_wave_graph.viewport.isXAxisBoundsManual = true

        data.iterator().forEach {
            compressionGraphSeries.appendData(DataPoint(it.key, -it.value), false, data.size, false)
            compression_wave_graph.viewport.setMaxX(it.key)
            compression_wave_graph.viewport.setMaxY(-it.value)
        }

        compression_wave_graph.addSeries(compressionGraphSeries)
    }


    companion object {
        @JvmStatic
        fun newInstance(videoSource: String) =
                GraphDebriefFragment().apply {
                    arguments = Bundle().apply {
                        putString(DebriefingActivity.videoSource, videoSource)
                    }
                }
    }
}
