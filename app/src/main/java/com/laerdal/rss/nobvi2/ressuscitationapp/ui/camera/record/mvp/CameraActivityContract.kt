package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxEvent
import com.otaliastudios.cameraview.CameraView

interface CameraActivityContract {

    interface View {
        fun startRecording()
        fun pauseRecording()
        fun stopRecording()
        fun proceedToDebriefVideoPreview(filePath: String)
        fun proceedToOriginalVideoPreview(filePath: String)
        fun playShutterSound()
        fun showLoader()
        fun hideLoader()
        fun showErrorMessage(errorMessage: String)
        fun showPreVideoChooser()
        fun sendCompressionDataToView(compressionData: Double)
    }


    interface Presenter {
        fun onRecordingClick()
        fun onStopClick()
        fun onThumbUpClick()
        fun onThumbDownClick()

        fun registerCameraView(camera: CameraView?)
        fun onPauseClick()
        fun processVideoWithMetadata()
        fun processDebriefVideo()
        fun goToVideoDebriefWithCPRCompression(debriefType: String)
        fun goToTaggedVideoDebrief(debriefType: String, filePath: String)
        fun goToTrimedVideoDebrief(debriefType: String, filePath: String)
        fun isFileToDisplay(): Boolean
        fun initiateFilePathValues()
        fun unsubscribe()
        fun subscribe()
    }
}