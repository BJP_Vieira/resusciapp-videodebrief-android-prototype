package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.CameraRecordingActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector


@Subcomponent(modules = [CameraRecordingActivityModule::class])
interface CameraRecordingActivityComponent : AndroidInjector<CameraRecordingActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<CameraRecordingActivity>()
}


