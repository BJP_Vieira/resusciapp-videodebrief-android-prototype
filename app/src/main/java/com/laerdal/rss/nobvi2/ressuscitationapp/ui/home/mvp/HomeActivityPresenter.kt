package com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.mvp

import android.content.Context
import android.content.Intent
import com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth.BLEService
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxBus
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxEvent
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.CameraRecordingActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class HomeActivityPresenter @Inject constructor(val view: HomeActivityContract.View) : HomeActivityContract.Presenter {

    @Inject
    lateinit var context: Context

    private var notificationDataDisposable: Disposable? = null


    override fun goToSessionView() {
        context.startActivity(Intent(context, CameraRecordingActivity::class.java))
    }

    override fun connectToLaerdalBLEDevice() {
        notificationDataDisposable = RxBus.listen(RxEvent.NotifyViewWithMessage::class.java).observeOn(AndroidSchedulers.mainThread()).subscribe {
            view.showMessageDialog(it.message)
        }
        context.startService(Intent(context, BLEService::class.java).setAction(BLEService.StartScan))

    }

    override fun unSubscribeEventListener() {
        notificationDataDisposable
        RxBus.unSubscribe(notificationDataDisposable)
    }
}