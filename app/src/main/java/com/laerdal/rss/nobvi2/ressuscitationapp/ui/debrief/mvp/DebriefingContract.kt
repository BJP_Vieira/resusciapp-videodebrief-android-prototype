package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.base.BaseView


interface DebriefingContract{

interface View
interface Presenter

}