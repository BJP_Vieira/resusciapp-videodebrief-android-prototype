package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.TagDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp.DebriefingContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp.DebriefingPresenter
import dagger.Module
import dagger.Provides

@Module
class TagDebriefFragmentModule {

    @Provides
    internal fun provideDetailFragmentView(tagDebriefFragment: TagDebriefFragment):  TagDebriefContract.View {
        return tagDebriefFragment
    }
    @Provides
    internal fun providePresenter(view:  TagDebriefContract.View ):  TagDebriefContract.Presenter  {
        return TagDebriefPresenter(view)
    }
}