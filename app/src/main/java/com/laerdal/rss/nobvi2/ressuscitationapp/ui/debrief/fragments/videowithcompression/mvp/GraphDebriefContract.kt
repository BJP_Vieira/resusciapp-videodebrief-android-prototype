package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.model.Tag
import io.realm.RealmList


interface GraphDebriefContract {

    interface View {
        fun sendCompressionDataToView(data: Map<Double, Double>)
        fun setTagsIntoVideoControllerView(tags: RealmList<Tag>?, toInt: Int)


    }

    interface Presenter {
        fun getCompressionDataFromTheBleService()
        fun setTags(videoSource: String?)
        fun unsubscribeFromEvents()
    }

}