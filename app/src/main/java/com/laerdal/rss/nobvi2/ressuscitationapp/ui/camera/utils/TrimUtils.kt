package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils

import android.content.Context
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.ffmpegcallback.FFmpegBinaryCallback
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.ffmpegcallback.FFmpegCallback
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.ForwardTimeShiftFromTagTime
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.ReverseTimeShiftFromTagTime
import java.io.File


object TrimUtils {

    fun loadEditorBinary(ffMpeg: FFmpeg) {
        ffMpeg.loadBinary(object : LoadBinaryResponseHandler() {
            override fun onFailure() {
            }

            override fun onSuccess() {
            }
        })
    }

    fun addMetadata(ffMpeg: FFmpeg, videoSourcePath: String, metaDataSource: String, finalVideoDestinationSourcePath: String, ffmpegBinaryCallback: FFmpegBinaryCallback) {

        val command: Array<String?> = arrayOf("-i", videoSourcePath, "-i", metaDataSource, "-map_metadata", "1", "-codec", "copy", finalVideoDestinationSourcePath)
        execVideoEditorCommand(ffMpeg, command, ffmpegBinaryCallback)
    }

    fun executeCuttingCommand(ffMpeg: FFmpeg, sourcePath: String, destinationName: String, tagTime: Long?, ffmpegBinaryCallback: FFmpegBinaryCallback) {
        if (tagTime == null) return
        val startMs = tagTime - ReverseTimeShiftFromTagTime
        val endMs = tagTime + ForwardTimeShiftFromTagTime

        val destinationFilePath = File(destinationName).absolutePath
        val trimCommand = arrayOf("-ss", "" + startMs / 1000, "-y", "-noautorotate", "-i", sourcePath, "-t", "" + (endMs - startMs) / 1000, "-c", "copy", destinationFilePath)
        execVideoEditorCommand(ffMpeg, trimCommand, ffmpegBinaryCallback)

    }


    fun executeMergeVideoCommand(ffMpeg: FFmpeg, destinationName: String, trimmedVideoPathsIterator: Iterator<String>, ffmpegBinaryCallback: FFmpegBinaryCallback) {

        val stringBuilder = StringBuilder()
        val filterComplex = StringBuilder()
        filterComplex.append("-filter_complex,")

        var index = 0
        trimmedVideoPathsIterator.forEach {
            stringBuilder.append("-i,$it,")
            filterComplex.append("[").append(index).append(":v").append(index).append("] [").append(index).append(":a").append(index).append("] ")
            index += 1

        }
        filterComplex.append("concat=n=").append(index).append(":v=1:a=1 [v] [a]")
        val inputCommand = stringBuilder.toString().split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val filterCommand = filterComplex.toString().split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val dest = File(destinationName)
        val destinationFile = dest.absolutePath
        val destinationCommand = arrayOf("-map", "[v]", "-map", "[a]", "-preset", "ultrafast", "-crf", "18", "-y", destinationFile)
        execVideoEditorCommand(ffMpeg, combine(inputCommand, filterCommand, destinationCommand), ffmpegBinaryCallback)
    }


    private fun execVideoEditorCommand(ffMpeg: FFmpeg, command: Array<String?>, callback: FFmpegBinaryCallback) {
        ffMpeg.killRunningProcesses()
        val processedVideoPath = command.iterator().asSequence().last()
        try {
            ffMpeg.execute(command, object : ExecuteBinaryResponseHandler() {
                override fun onFailure(s: String) {
                    callback.onError()
                }

                override fun onSuccess(s: String) {
                    processedVideoPath?.let { callback.onSuccess(it) }
                }


                override fun onProgress(s: String) {
                }

                override fun onStart() {

                }

                override fun onFinish() {

                }
            })
        } catch (e: FFmpegCommandAlreadyRunningException) {
            e.printStackTrace()

        }
    }

    private fun combine(arg1: Array<String>, arg2: Array<String>, arg3: Array<String>): Array<String?> {
        val result = arrayOfNulls<String>(arg1.size + arg2.size + arg3.size)
        System.arraycopy(arg1, 0, result, 0, arg1.size)
        System.arraycopy(arg2, 0, result, arg1.size, arg2.size)
        System.arraycopy(arg3, 0, result, arg1.size + arg2.size, arg3.size)
        return result
    }


}