package com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.mvp


import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp.DebriefingContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp.DebriefingPresenter
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.HomeActivity


import dagger.Module
import dagger.Provides

@Module
class HomeActivityModule {

    @Provides
    internal fun provideView(view: HomeActivity): HomeActivityContract.View {
        return view
    }

    @Provides
    internal fun providePresenter(mainView:  HomeActivityContract.View ):  HomeActivityContract.Presenter  {
        return HomeActivityPresenter(mainView)
    }
}

