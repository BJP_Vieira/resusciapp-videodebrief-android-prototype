package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.ffmpegcallback

import com.github.hiteshsondhi88.libffmpeg.FFmpeg

interface FFmpegCallback {
    fun onError()
    fun onSuccess(ffMpeg: FFmpeg)
}