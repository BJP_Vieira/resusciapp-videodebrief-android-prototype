package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp

import android.media.MediaMetadataRetriever
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Video
import io.realm.Realm
import io.realm.kotlin.where
import wseemann.media.FFmpegMediaMetadataRetriever
import javax.inject.Inject


class TagDebriefPresenter @Inject constructor(val view: TagDebriefContract.View) : TagDebriefContract.Presenter {
    @Inject
    lateinit var realm: Realm

    override fun prepareTagArray(videoSource: String) {
        val video = realm.where<Video>().equalTo("path", videoSource).findFirst()
        val tags = video?.tags


        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(videoSource)
        val videoDuration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
        retriever.release()
        val ffmpegMediaRetriever = FFmpegMediaMetadataRetriever()
        ffmpegMediaRetriever.setDataSource(videoSource)

        view.setTagsIntoVideoControllerView(tags, videoDuration.toInt())
    }
}
