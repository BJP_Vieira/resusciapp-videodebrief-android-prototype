package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.tag_video_item.view.*
import android.provider.MediaStore
import android.media.ThumbnailUtils
import android.graphics.Bitmap
import com.laerdal.rss.nobvi2.ressuscitationapp.R
import com.laerdal.rss.nobvi2.ressuscitationapp.model.TagClassifier
import com.laerdal.rss.nobvi2.ressuscitationapp.model.TrimVideo
import javax.inject.Inject


/**
 * Adapter for the planet data used in our drawer menu.
 */
class TrimVideoAdapter(
        private val sourcePathData: List<TrimVideo>,
        private val listener: OnItemClickListener,
        private val context: Context?
) : RecyclerView.Adapter<TrimVideoAdapter.ViewHolder>() {


    /**
     * Interface for receiving click events from cells.
     */
    interface OnItemClickListener {
        fun onClick(tag: TrimVideo, hasNext: Boolean, position: Int)
    }


    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.tag_video_item, p0, false))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            val preview = createVideoThumbNail(sourcePathData[position].path)
            holder.videoThumbnail.setImageBitmap(preview)

            if (sourcePathData[position].tag?.classifier == TagClassifier.Positive.value) {
                holder.fadeLayer.background = context?.getDrawable(R.color.redWithTransparency)
            } else {
                holder.fadeLayer.background = context?.getDrawable(R.color.greenWithTransparency)

            }

            holder.container.setOnClickListener {
                listener.onClick(sourcePathData[position], sourcePathData.iterator().hasNext(), position)
            }
        }

    }

    override fun getItemCount() = sourcePathData.size


    private fun createVideoThumbNail(path: String): Bitmap {
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val videoThumbnail = view.video_thumbnail!!
        val container = view.video_item!!
        val playIc = view.video_thumbnail_play_ic!!
        val fadeLayer = view.fadelayer!!

    }
}


