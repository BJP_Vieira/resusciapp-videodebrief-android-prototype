package com.laerdal.rss.nobvi2.ressuscitationapp.model

import android.graphics.Bitmap
import android.provider.MediaStore
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import java.sql.Timestamp

open class Tag:RealmObject(){

    var tagTimestamp: Long = 0
    var classifier:Int = -1

    @LinkingObjects("tags")
    val owners: RealmResults<Video>? = null
}