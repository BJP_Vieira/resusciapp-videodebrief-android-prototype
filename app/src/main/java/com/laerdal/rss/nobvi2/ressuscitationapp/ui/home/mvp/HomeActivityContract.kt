package com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.base.BaseView


interface HomeActivityContract{

interface View{
    fun showMessageDialog(message: String)
}
interface Presenter{
    fun goToSessionView()
    fun connectToLaerdalBLEDevice()
    fun unSubscribeEventListener()
}

}