package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.GraphDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp.TagDebriefContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp.TagDebriefPresenter
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
 class GraphDebriefFragmentModule {

    @Provides
    internal fun provideDetailFragmentView(graphDebriefFragment: GraphDebriefFragment): GraphDebriefContract.View {
        return graphDebriefFragment
    }

    @Provides
    internal fun providePresenter(view: GraphDebriefContract.View): GraphDebriefContract.Presenter {
        return GraphDebriefPresenter(view)
    }

}