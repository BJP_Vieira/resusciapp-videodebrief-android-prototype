package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.mvp


import com.laerdal.rss.nobvi2.ressuscitationapp.model.TrimVideo
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Video
import io.realm.Realm
import io.realm.kotlin.where
import javax.inject.Inject


class TrimmedDebriefPresenter @Inject constructor(val view: TrimmedDebriefContract.View) : TrimmedDebriefContract.Presenter {
@Inject
lateinit var realm: Realm

    override fun getTrimmedVideos(videoSource: String) {

        val video = realm.where<Video>().equalTo("path", videoSource).findFirst()
        val trimmedVideos = video?.trimmedVideos as List<TrimVideo>


        if (trimmedVideos.isEmpty()) view.showErrorMessage("This Session does not contain any Flags")

        view.setVideosIntoView(trimmedVideos)
    }


}