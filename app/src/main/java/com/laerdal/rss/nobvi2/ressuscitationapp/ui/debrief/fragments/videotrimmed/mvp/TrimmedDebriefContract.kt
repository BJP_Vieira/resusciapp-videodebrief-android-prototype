package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.model.TrimVideo


interface TrimmedDebriefContract {

    interface View {
        fun setVideosIntoView(videoArray: List<TrimVideo>)
        fun showErrorMessage(errorMessage: String)
    }

    interface Presenter {
        fun getTrimmedVideos(videoSource: String)
    }

}