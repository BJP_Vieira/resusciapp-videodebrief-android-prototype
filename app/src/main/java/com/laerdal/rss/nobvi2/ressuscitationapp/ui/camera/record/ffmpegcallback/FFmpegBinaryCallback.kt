package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.ffmpegcallback

interface FFmpegBinaryCallback {
    fun onError()
    fun onSuccess(processedVideoPath:String)
}