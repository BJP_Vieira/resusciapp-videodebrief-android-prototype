package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.mvp

import android.content.Context
import android.content.Intent
import android.media.MediaMetadataRetriever
import com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth.BLEService
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxBus
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxEvent
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Video
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.realm.Realm
import io.realm.kotlin.where
import wseemann.media.FFmpegMediaMetadataRetriever
import javax.inject.Inject


class GraphDebriefPresenter @Inject constructor(val view: GraphDebriefContract.View) : GraphDebriefContract.Presenter {

    @Inject
    lateinit var context: Context

    private var notificationDataDisposable: Disposable? = null



    @Inject
    lateinit var realm: Realm


    override fun setTags(videoSource:String?) {
        val video = realm.where<Video>().equalTo("path", videoSource).findFirst()
        val tags = video?.tags


        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(videoSource)
        val videoDuration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
        retriever.release()
        val ffmpegMediaRetriever = FFmpegMediaMetadataRetriever()
        ffmpegMediaRetriever.setDataSource(videoSource)

        view.setTagsIntoVideoControllerView(tags, videoDuration.toInt())
    }

    override fun getCompressionDataFromTheBleService() {
        context.startService(Intent(context, BLEService::class.java).setAction(BLEService.RetrieveData))
        notificationDataDisposable = RxBus.listen(RxEvent.EventPublishCompressionData::class.java).observeOn(AndroidSchedulers.mainThread()).subscribe {
            view.sendCompressionDataToView(it.dataList)
        }
    }

    override fun unsubscribeFromEvents() {
        RxBus.unSubscribe(notificationDataDisposable)
    }
}