package com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.HomeActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector


@Subcomponent(modules = [HomeActivityModule::class])
interface HomeActivityComponent : AndroidInjector<HomeActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<HomeActivity>()
}


