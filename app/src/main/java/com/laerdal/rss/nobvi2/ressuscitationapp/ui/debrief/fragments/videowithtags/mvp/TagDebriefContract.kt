package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp

import com.laerdal.rss.nobvi2.ressuscitationapp.model.Tag
import io.realm.RealmList


interface TagDebriefContract {

    interface View {
        fun setTagsIntoVideoControllerView(tagArray: RealmList<Tag>?, videoDuration: Int)
        fun showErrorMessage(errorMessage: String)

    }

    interface Presenter {
        fun prepareTagArray(videoSource:String)
    }

}