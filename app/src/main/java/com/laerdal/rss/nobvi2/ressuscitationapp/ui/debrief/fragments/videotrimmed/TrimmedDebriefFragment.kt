package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.laerdal.rss.nobvi2.ressuscitationapp.R
import com.laerdal.rss.nobvi2.ressuscitationapp.model.TrimVideo
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.customviews.SpeedyLinearLayoutManager
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.mvp.TrimmedDebriefContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.mvp.TrimmedDebriefPresenter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_trimmed_debrief.*
import java.io.IOException
import javax.inject.Inject

class TrimmedDebriefFragment : DaggerFragment(), TrimmedDebriefContract.View, TrimVideoAdapter.OnItemClickListener {

    @Inject
    lateinit var presenter: TrimmedDebriefPresenter

    private lateinit var trimmedVideos:List<TrimVideo>


    private var nextToPlay = 0
    private var currentToPlay = 0
    private var previous = 0
    private var videoPosition = 0

    private var videoSource: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            videoSource = it.getString(DebriefingActivity.videoSource)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_trimmed_debrief, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = context?.let { SpeedyLinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false) }
        list_view.layoutManager = layoutManager
        context?.resources?.getColor(R.color.color_transparent)?.let { list_view.setBackgroundColor(it) }


        videoSource?.let { presenter.getTrimmedVideos(it) }


        videoView.setOnCompletionListener {
            val itemView = list_view.findViewHolderForAdapterPosition(nextToPlay)?.itemView?.findViewById<View>(R.id.video_item)
            val fadeLayer = list_view.findViewHolderForAdapterPosition(previous)?.itemView?.findViewById<View>(R.id.fadelayer)

            Handler().postDelayed({
                fadeLayer?.visibility = View.VISIBLE
                itemView?.performClick()
            }, 1)

            Handler().postDelayed({ list_view.layoutManager?.smoothScrollToPosition(list_view, null, nextToPlay) }, 100)

            if (currentToPlay == trimmedVideos.size - 1) {
                bt_play.background = context?.getDrawable(R.drawable.ic_play)
            }
        }


        bt_play.setOnClickListener {
            if (videoView.isPlaying) {
                videoPosition = videoView.currentPosition
                videoView.pause()
                it.background = context?.getDrawable(R.drawable.ic_play)
            } else {
                if (currentToPlay == 0) {
                    Handler().postDelayed({ list_view.findViewHolderForAdapterPosition(0)?.itemView?.findViewById<View>(R.id.fadelayer)?.visibility = View.INVISIBLE }, 1)
                    nextToPlay = 1
                } else if (currentToPlay == trimmedVideos.size - 1) {
                    Handler().postDelayed({ list_view.findViewHolderForAdapterPosition(0)?.itemView?.findViewById<View>(R.id.fadelayer)?.visibility = View.INVISIBLE }, 1)
                    Handler().postDelayed({ list_view.layoutManager?.smoothScrollToPosition(list_view, null, 0) }, 10)
                    Handler().postDelayed({ list_view.findViewHolderForAdapterPosition(currentToPlay)?.itemView?.findViewById<View>(R.id.fadelayer)?.visibility = View.VISIBLE }, 100)

                    currentToPlay = 0
                    previous = 0
                    nextToPlay = 1
                    videoView.setVideoPath(trimmedVideos[0].path)

                }
                it.background = context?.getDrawable(R.drawable.ic_pause)
                if (videoPosition > 0) {
                    videoView.seekTo(videoPosition)
                    videoPosition = 0
                } else {
                }


                videoView.start()
            }
        }


    }


    override fun setVideosIntoView(videoArray: List<TrimVideo>) {
        trimmedVideos = videoArray
        list_view.adapter = TrimVideoAdapter(videoArray, this,context)
        videoView.setVideoPath(videoArray[0].path)

    }

    override fun onClick(tag: TrimVideo, hasNext: Boolean, position: Int) {
        try {

            videoView.setVideoPath(tag.path)
            bt_play.background = context?.getDrawable(R.drawable.ic_pause)
            Handler().postDelayed({ list_view.findViewHolderForAdapterPosition(position)?.itemView?.findViewById<View>(R.id.fadelayer)?.visibility = View.INVISIBLE }, 1)
            Handler().postDelayed({ list_view.findViewHolderForAdapterPosition(previous)?.itemView?.findViewById<View>(R.id.fadelayer)?.visibility = View.VISIBLE }, 1)
            previous = currentToPlay
            currentToPlay = position
            if (hasNext) {
                nextToPlay = position + 1
            }
            videoView.start()
        } catch (e: SecurityException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun showErrorMessage(errorMessage: String) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
    }

    companion object {
        @JvmStatic
        fun newInstance(videoSource: String) =
                TrimmedDebriefFragment().apply {
                    arguments = Bundle().apply {
                        putString(DebriefingActivity.videoSource, videoSource)
                    }
                }
    }
}
