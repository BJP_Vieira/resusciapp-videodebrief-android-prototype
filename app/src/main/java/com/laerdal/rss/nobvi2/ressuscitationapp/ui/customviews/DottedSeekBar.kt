package com.laerdal.rss.nobvi2.ressuscitationapp.ui.customviews

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.widget.SeekBar

import com.laerdal.rss.nobvi2.ressuscitationapp.R
import android.graphics.drawable.BitmapDrawable
import android.os.Build.VERSION_CODES.N
import android.util.DisplayMetrics
import android.view.ViewGroup
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Tag
import com.laerdal.rss.nobvi2.ressuscitationapp.model.TagClassifier


class DottedSeekBar : SeekBar {

    private var tags: MutableList<Tag>? = null
    private var videoDuration:Int=0

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs)
    }

    /**
     * Initializes Seek bar extended attributes from xml
     *
     * @param attributeSet [AttributeSet]
     */
    private fun init(attributeSet: AttributeSet?) {
    }


    fun setTags(tagSet: MutableList<Tag>, mVideoDuration: Int) {
        tags = tagSet
        videoDuration=mVideoDuration
        max=videoDuration
        invalidate()
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap {

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    @Synchronized
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if(tags==null) return

        val lp = layoutParams as ViewGroup.MarginLayoutParams
        val margins: Float = (lp.rightMargin + lp.leftMargin).toFloat()



        for (tag in tags!!) {

            val bitmap:Bitmap = if(tag.classifier==TagClassifier.Positive.value) {
                drawableToBitmap(resources.getDrawable(R.drawable.ic_greenish_dot))
            }else{
                drawableToBitmap(resources.getDrawable(R.drawable.ic_red_dot))

            }

            val withCorrection = bitmap!!.width / 2.toFloat()
            val seekBarSizeCorrection = margins + withCorrection
            val seekBarSizeCorrectionPercent = (seekBarSizeCorrection) * 100 / measuredWidth.toFloat()

            val realWith = measuredWidth - seekBarSizeCorrection

            val startTime = tag.tagTimestamp
            val tagTimeInPercent = startTime * 100 / videoDuration

            val position= tagTimeInPercent
            var newPosition = position
            if (position == 0L) newPosition = (((lp.rightMargin + withCorrection) * 100 / measuredWidth).toLong())
            val location = ((newPosition * realWith) / 100)
            val finalLocation = location - seekBarSizeCorrectionPercent


            canvas.drawBitmap(bitmap, finalLocation, 3F, null)
        }

    }
}