package com.laerdal.rss.nobvi2.ressuscitationapp.db

import android.provider.MediaStore
import com.laerdal.rss.nobvi2.ressuscitationapp.model.TrimmedVideo
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Video
import io.realm.Realm
import io.realm.kotlin.createObject


class DBCrud {

    fun writeVideo(realm: Realm, video: Video): Int {
        val maxId = realm.where(Video::class.java).max("id")
        val nextId = if (maxId == null) 1 else maxId.toInt() + 1

        realm.executeTransaction {
            video.id= nextId.toLong()
            it.insertOrUpdate(video)
        }
        return nextId
    }

}