package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record

import android.content.Context
import java.io.File


object  CameraRecordingUtils {

    fun getVideoFilePath(context: Context?): String {
        val filename = "${System.currentTimeMillis()}.mp4"
        val dir = context?.getExternalFilesDir(null)
        return if (dir == null) {
            filename
        } else {
            "${dir.absolutePath}/$filename"
        }
    }

    fun getVideoRootPath(context: Context?): String {
        val dir = context?.getExternalFilesDir(null)
        val finalDir="${dir?.absolutePath}"
        if(!File(finalDir).exists()){
            File(finalDir).mkdir()
        }
        return "${dir?.absolutePath}/"
        }
}