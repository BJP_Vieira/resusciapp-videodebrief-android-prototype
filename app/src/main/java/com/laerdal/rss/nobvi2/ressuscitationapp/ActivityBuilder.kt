package com.laerdal.rss.nobvi2.ressuscitationapp



import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.CameraRecordingActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.mvp.CameraRecordingActivityModule
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp.DebriefingModule
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.HomeActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.home.mvp.HomeActivityModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [CameraRecordingActivityModule::class])
    internal abstract fun bindCameraRecordingActivity(): CameraRecordingActivity

    @ContributesAndroidInjector(modules = [DebriefingModule::class, FragmentBuilder::class])
    internal abstract fun bindDebriefingActivity(): DebriefingActivity

    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    internal abstract fun bindHomeActivity(): HomeActivity

}

