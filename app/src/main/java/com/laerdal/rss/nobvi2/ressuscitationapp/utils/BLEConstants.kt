@file:JvmName("BLEConstants")

package com.laerdal.rss.nobvi2.ressuscitationapp.utils

import android.os.ParcelUuid
import java.util.*


const val LaerdalManufacturerId = 0x01CA
const val LaerdalManufacturerIdString = "<ca0102>"
const val ApplicationMode = 0x04
const val BootMode = 0x02

val Battery = ParcelUuid.fromString("0000180f-0000-1000-8000-00805f9b34fb")
val DeviceInformation = ParcelUuid.fromString("0000180a-0000-1000-8000-00805f9b34fb")
val Dfu = ParcelUuid.fromString("00001530-1212-EFDE-1523-785FEABCD123")
const val Base_UUID1_old = "-1212-efde-1523-785feabcd123"
const val Base_UUID1 = "-d746-4092-84e7-dad34863fe4a"
val LaerdalDevice = ParcelUuid.fromString("000001e0$Base_UUID1")


val AuthenticateCharacteristicGuid = UUID.fromString("000001b1$Base_UUID1")
val AuthenticationStatusCharacteristicGuid = UUID.fromString("000001b2$Base_UUID1")
val AuthenticationServiceGuid = UUID.fromString("000001b10$Base_UUID1")

val Compression = UUID.fromString("00000026$Base_UUID1")
val Session = UUID.fromString("00000126$Base_UUID1")
val Feedback = UUID.fromString("000001a0$Base_UUID1")
val Ventilation = UUID.fromString("00500050$Base_UUID1")

val CompressionWave = UUID.fromString("00000027$Base_UUID1")
val CompressionCompleteEvent = UUID.fromString("00000028$Base_UUID1")
val CompressionInactivityTime = UUID.fromString("00000030$Base_UUID1")

val SessionControl = UUID.fromString("00000127$Base_UUID1")

val ModeSession = UUID.fromString("00000130$Base_UUID1")
val Scoring = UUID.fromString("00000131$Base_UUID1")


enum class SessionType (val value: Byte){
    Disabled(0x00.toByte()),
    RaceMode(0x01.toByte()),
    ClassRoomMode(0x02.toByte())
}


val AuthenticationKey = byteArrayOf(
        0x5c.toByte(),
        0x0e.toByte(),
        0xb9.toByte(),
        0xbe.toByte(),
        0x03.toByte(),
        0xd2.toByte(),
        0x60.toByte(),
        0x09.toByte(),
        0x6d.toByte(),
        0x3e.toByte(),
        0x1f.toByte(),
        0x0c.toByte(),
        0x80.toByte(),
        0x26.toByte(),
        0xc8.toByte(),
        0x10.toByte(),
        0x73.toByte(),
        0xcd.toByte(),
        0x2e.toByte(),
        0xa2.toByte())


enum class DeviceType(val device: Int) {
    Newborn(0x06),
    LittleAnne(0x02),
    LittleJunior(0x0f),
    ResusciJunior(0x07),
    ResusciAnne(0x08)
}

enum class AuthenticationStatus(val status: Byte) {
    Failed(0xaa.toByte()),
    Success(0x55.toByte()),
}


enum class SessionControlCommand(val sessionValue: Byte) {
    Stop(0x00.toByte()),
    Abort(0x01.toByte()),
    Pause(0x02.toByte()),
    Start(0x03.toByte()),
    AutoStart(0x04.toByte()),
    Config(0xff.toByte())
}

enum class CompressionsOnly(val value: Byte) {
    //255,0
    Compressions(0xff.toByte()),
    Ventilations(0x00.toByte())
}
