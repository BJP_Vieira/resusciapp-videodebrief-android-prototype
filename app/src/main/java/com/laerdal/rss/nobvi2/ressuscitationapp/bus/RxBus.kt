package com.laerdal.rss.nobvi2.ressuscitationapp.bus

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject


object RxBus {

    private val publisher = PublishSubject.create<Any>()

    fun publish(event: Any) {
        publisher.onNext(event)
    }

    // Listen should return an Observable and not the publisher
    // Using ofType we filter only events that match that class type
    fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)

    fun unSubscribe(disposable: Disposable?) {
        if (disposable?.isDisposed != null && !disposable.isDisposed)
            disposable.dispose()
    }

}