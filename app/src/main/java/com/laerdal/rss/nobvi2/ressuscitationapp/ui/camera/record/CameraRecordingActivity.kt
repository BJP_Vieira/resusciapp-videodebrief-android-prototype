package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle

import com.laerdal.rss.nobvi2.ressuscitationapp.R
import kotlinx.android.synthetic.main.activity_camera.*
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.mvp.CameraPresenter
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils.blink
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.PERMISSIONS
import android.graphics.Color
import android.graphics.Point
import android.os.Build
import android.os.Handler

import com.otaliastudios.cameraview.VideoCodec
import com.otaliastudios.cameraview.SessionType
import com.otaliastudios.cameraview.VideoQuality
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils.flash
import android.widget.TextView
import android.support.design.widget.Snackbar
import android.view.*
import android.view.animation.TranslateAnimation
import com.jjoe64.graphview.GridLabelRenderer
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.laerdal.rss.nobvi2.ressuscitationapp.R.id.*
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxEvent
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.mvp.CameraActivityContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.Prefs
import dagger.android.support.DaggerAppCompatActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_camera.view.*
import kotlinx.android.synthetic.main.alert_dialog.view.*
import kotlinx.android.synthetic.main.fragment_graph_debrief.*
import library.minimize.com.chronometerpersist.ChronometerPersist
import javax.inject.Inject


class CameraRecordingActivity : DaggerAppCompatActivity(), CameraActivityContract.View {

    @Inject
    lateinit var presenter: CameraPresenter
    private lateinit var prefs: Prefs

    private var realm = Realm.getDefaultInstance()
    private var chronometerPersist: ChronometerPersist? = null
    private var initialAnimationPosition = 0F
    private var conversionFactor = 0F;
    private var dialog: AlertDialog? = null
    override fun showLoader() {
        bt_rec.isEnabled = false
        contentProgressLoader.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        bt_rec.isEnabled = true
        contentProgressLoader.visibility = View.GONE
    }

    override fun showErrorMessage(errorMessage: String) {
        val snackbar = Snackbar.make(recordActivitytLayout, errorMessage, Snackbar.LENGTH_SHORT)
        val snackbarView = snackbar.view
        val textView = snackbarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.YELLOW)
        snackbar.show()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        camera.setLifecycleOwner(this)
        camera?.sessionType = SessionType.VIDEO
        camera.videoCodec = VideoCodec.H_264
        camera.videoQuality = VideoQuality.MAX_1080P
        val sharedPreferences = getSharedPreferences("ChronometerSample", Context.MODE_PRIVATE)
        chronometerPersist = ChronometerPersist.getInstance(chronometer_view, sharedPreferences)
        chronometerPersist?.hourFormat(true)

        presenter.registerCameraView(camera)

        bt_rec.setOnClickListener {
            if (!hasPermissionsGranted(PERMISSIONS))
                askForPermissions()
            else {
                if (!camera.isStarted) camera.start()
                presenter.onRecordingClick()
                chronometerPersist?.startChronometer()
            }
        }

        bt_pause.setOnClickListener {
            presenter.onPauseClick()
        }
        bt_stop.setOnClickListener {
            presenter.onStopClick()
            updatePauseStop()
            chronometerPersist?.stopChronometer()

        }

        bt_tag_down.setOnClickListener {
            presenter.onThumbDownClick()
        }

        bt_tag_up.setOnClickListener {
            presenter.onThumbUpClick()
        }


    }

    override fun pauseRecording() {
        updatePauseStop()
    }

    override fun stopRecording() {
        updatePauseStop()
    }

    override fun playShutterSound() {
        flashView.flash(flashView)
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe()
        conversionFactor = getViewDataConversionFactor()
        if (!camera.isStarted) {
            camera.start()
        }

        if (presenter.isFileToDisplay()) {
            presenter.initiateFilePathValues()
            showPreVideoChooser()
        }
    }

    override fun onPause() {
        super.onPause()
        camera.stop()
        presenter.unsubscribe()


    }

    override fun onDestroy() {
        super.onDestroy()
        camera.destroy()
        realm.close()
    }

    override fun startRecording() {
        bt_rec.visibility = View.INVISIBLE
        img_blinking.visibility = View.VISIBLE
        img_blinking.blink()
        bt_tag_down.visibility = View.VISIBLE
        bt_tag_up.visibility = View.VISIBLE
        bt_stop.visibility = View.VISIBLE
        chronometer_view.visibility = View.VISIBLE
        stack_view.visibility = View.VISIBLE

    }


    private fun getViewHeight(view: View): Int {
        val wm = view.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay


        val deviceHeight = if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val size = Point()
            display.getSize(size)
            size.y
        } else {
            display.height
        }

        val deviceWidth = if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val size = Point()
            display.getSize(size)
            size.x
        } else {
            display.width
        }

        val widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST)
        val heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(widthMeasureSpec, heightMeasureSpec)
        return view.measuredWidth
    }

    override fun sendCompressionDataToView(compressionData: Double) {


        val translate = conversionFactor * compressionData

        if (initialAnimationPosition == translate.toFloat()) return

        val anim = TranslateAnimation(0F, 0F, initialAnimationPosition, translate.toFloat())
        anim.duration = 0
        anim.fillAfter = true
        moving_stack.startAnimation(anim)
        initialAnimationPosition = translate.toFloat()

    }

    private fun getViewDataConversionFactor(): Float {
        val stack = getViewHeight(stack_view)
        val topLimit = getViewHeight(view)
        val lowerLimit = getViewHeight(view2)
        val gap = stack - (topLimit + lowerLimit)

        return (stack_view.measuredHeightAndState / 51).toFloat()
    }

    private fun updatePauseStop() {
        img_blinking.clearAnimation()
        img_blinking.visibility = View.INVISIBLE
        bt_rec.visibility = View.VISIBLE
        bt_tag_down.visibility = View.INVISIBLE
        bt_tag_up.visibility = View.INVISIBLE
        bt_stop.visibility = View.INVISIBLE
        bt_pause.visibility = View.INVISIBLE
        chronometer_view.visibility = View.INVISIBLE
        stack_view.visibility = View.INVISIBLE

    }


    private fun askForPermissions() {
        val rationale = "Please provide access to following so that you can ..."
        val options = Permissions.Options()
                .setRationaleDialogTitle("Info")
                .setSettingsDialogTitle("Warning")

        Permissions.check(this, PERMISSIONS, rationale, options, object : PermissionHandler() {
            override fun onGranted() {
            }

            override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {


            }
        })
    }

    private fun hasPermissionsGranted(permissions: Array<String>) =
            permissions.none {
                ContextCompat.checkSelfPermission((this as Activity), it) != PackageManager.PERMISSION_GRANTED
            }

    override fun proceedToOriginalVideoPreview(filePath: String) {
        presenter.goToTaggedVideoDebrief(DebriefingActivity.TagVideoFragment, filePath)
    }

    override fun proceedToDebriefVideoPreview(filePath: String) {
        presenter.goToTaggedVideoDebrief(DebriefingActivity.TrimVideoFragment, filePath)

    }

    override fun onBackPressed() {
        if (dialog != null && dialog!!.isShowing)
            dialog?.dismiss()
        if (!camera.isCapturingVideo)
            super.onBackPressed()
    }

    override fun showPreVideoChooser() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog, null)
        val builder = AlertDialog.Builder(this, R.style.CustomAlertDialog).setView(dialogView)

        dialog = builder.create()
        dialog?.show()
        dialog?.setCanceledOnTouchOutside(false)

        dialogView.bt_full_with_flag.setOnClickListener {
            presenter.processVideoWithMetadata()
            dialog?.dismiss()
        }
        dialogView.bt_only_flagged.setOnClickListener {
            presenter.processDebriefVideo()
            dialog?.dismiss()
        }

        dialogView.bt_full_with_compressions.setOnClickListener {
            dialog?.dismiss()
            presenter.goToVideoDebriefWithCPRCompression(DebriefingActivity.CompressionGraphFragment)
        }

    }


}

