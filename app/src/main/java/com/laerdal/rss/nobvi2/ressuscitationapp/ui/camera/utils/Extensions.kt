package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils

import android.support.annotation.AnimRes
import android.view.View
import android.view.animation.*
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_camera.*
import kotlin.text.Typography.times


fun View.blink(
        times: Int = Animation.INFINITE,
        duration: Long = 500L,
        offset: Long = 250L,
        minAlpha: Float = 0.5f,
        maxAlpha: Float = 1.0f,
        repeatMode: Int = Animation.REVERSE
) {
    startAnimation(AlphaAnimation(minAlpha, maxAlpha).also {
        it.duration = duration
        it.startOffset = offset
        it.repeatMode = repeatMode
        it.repeatCount = times
    })
}


fun View.flash(view: View, minAlpha: Float = 0.0f, maxAlpha: Float = 1.0f, duration: Long = 50L) {
    startAnimation(AlphaAnimation(minAlpha, maxAlpha).also {
        it.duration = duration
        it.setAnimationListener(
                object : Animation.AnimationListener {
                    override fun onAnimationRepeat(animation: Animation?) {
                    }

                    override fun onAnimationStart(animation: Animation?) {
                        view.visibility = View.VISIBLE
                    }

                    override fun onAnimationEnd(animation: Animation?) {
                        view.visibility = View.GONE
                    }
                })
    })
}

fun View.fadeOut(view: View,
                 duration: Long = 10L,
                 offset: Long = 0L,
                 minAlpha: Float = 0f,
                 maxAlpha: Float = 1.0f
) {
    startAnimation(AlphaAnimation(maxAlpha, minAlpha).also {
        it.duration = duration
        it.startOffset = offset
        it.interpolator = AccelerateInterpolator()
        it.setAnimationListener(
                object : Animation.AnimationListener {
                    override fun onAnimationRepeat(animation: Animation?) {
                    }

                    override fun onAnimationStart(animation: Animation?) {
                    }

                    override fun onAnimationEnd(animation: Animation?) {
                        view.visibility = View.GONE
                    }
                })
    })
}

fun View.fadeIn(
        duration: Long = 200L,
        offset: Long = 50L,
        minAlpha: Float = 0f,
        maxAlpha: Float = 1.0f
) {
    startAnimation(AlphaAnimation(maxAlpha, minAlpha).also {
        it.duration = duration
        it.startOffset = offset
        it.interpolator = DecelerateInterpolator()
    })
}





