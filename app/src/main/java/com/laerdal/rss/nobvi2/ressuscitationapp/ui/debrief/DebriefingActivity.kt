package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import com.laerdal.rss.nobvi2.ressuscitationapp.BuildConfig
import com.laerdal.rss.nobvi2.ressuscitationapp.R
import com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth.BLEService
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.TrimmedDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.GraphDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.TagDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp.DebriefingContract
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp.DebriefingPresenter

import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject


class DebriefingActivity : DaggerAppCompatActivity(), DebriefingContract.View, ServiceConnection {

    private var serviceBound: Boolean = false

    @Inject
    lateinit var presenter: DebriefingPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debriefing)

        serviceBound = bindService(Intent(this, BLEService::class.java), this, Context.BIND_AUTO_CREATE)

        val action = intent.action
        val bundle = intent.extras
        val videoSource = bundle?.getString(videoSource)

        when (action) {
            CompressionGraphFragment -> if (videoSource != null) {
                goToGraphDebriefFragment(videoSource)
            }
            TagVideoFragment -> if (videoSource != null) {
                goToTagDebriefFragment(videoSource)
            }
            TrimVideoFragment -> if (videoSource != null) {
                goToTrimmedVideoFragment(videoSource)
            }
        }

    }


    override fun onDestroy() {
        unbindService(this)
        super.onDestroy()
    }

    private fun goToGraphDebriefFragment(videoSource: String) {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, GraphDebriefFragment.newInstance(videoSource))
                .commitAllowingStateLoss()
    }


    private fun goToTagDebriefFragment(videoSource: String) {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, TagDebriefFragment.newInstance(videoSource))
                .commitAllowingStateLoss()
    }

    private fun goToTrimmedVideoFragment(videoSource: String) {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, TrimmedDebriefFragment.newInstance(videoSource))
                .commitAllowingStateLoss()
    }


    override fun onServiceDisconnected(name: ComponentName?) {
        serviceBound = false
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        //TODO is there than can be done here?
    }

    companion object {
        private const val PREFIX = BuildConfig.APPLICATION_ID + ".DebriefingActivity:"

        const val CompressionGraphFragment = PREFIX + "CompressionGraph"
        const val TagVideoFragment = PREFIX + "TagVideoFragment"
        const val TrimVideoFragment = PREFIX + "TrimVideoFragment"
        const val videoSource = PREFIX + "VideoSource"


    }
}
