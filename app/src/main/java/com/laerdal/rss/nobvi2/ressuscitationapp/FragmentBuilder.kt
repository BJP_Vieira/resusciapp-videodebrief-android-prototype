package com.laerdal.rss.nobvi2.ressuscitationapp

import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.GraphDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithcompression.mvp.GraphDebriefFragmentModule
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.TrimmedDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videotrimmed.mvp.TrimmedDebriefFragmentModule
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.TagDebriefFragment
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.fragments.videowithtags.mvp.TagDebriefFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [GraphDebriefFragmentModule::class])
    internal abstract fun provideGraphDebriefFragmentFactory(): GraphDebriefFragment

    @ContributesAndroidInjector(modules = [TagDebriefFragmentModule::class])
    internal abstract fun provideTagDebriefFragmentFactory(): TagDebriefFragment

    @ContributesAndroidInjector(modules = [TrimmedDebriefFragmentModule::class])
    internal abstract fun provideTrimmedDebriefFragmentFactory(): TrimmedDebriefFragment

}