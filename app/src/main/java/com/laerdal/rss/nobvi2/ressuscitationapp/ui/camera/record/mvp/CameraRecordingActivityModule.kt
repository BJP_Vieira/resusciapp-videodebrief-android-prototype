package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.mvp

import android.content.Context
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.CameraRecordingActivity


import dagger.Module
import dagger.Provides

@Module
class CameraRecordingActivityModule {

    @Provides
    internal fun provideView(cameraRecordingActivity: CameraRecordingActivity): CameraActivityContract.View {
        return cameraRecordingActivity
    }

    @Provides
    internal fun providePresenter(mainView: CameraActivityContract.View): CameraActivityContract.Presenter {
        return CameraPresenter(mainView)
    }
}

