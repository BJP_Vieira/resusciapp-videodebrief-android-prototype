package com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.mvp


import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity


import dagger.Module
import dagger.Provides

@Module
class DebriefingModule {

    @Provides
    internal fun provideView(view: DebriefingActivity): DebriefingContract.View {
        return view
    }

    @Provides
    internal fun providePresenter(mainView:  DebriefingContract.View ):  DebriefingContract.Presenter  {
        return DebriefingPresenter(mainView)
    }
}

