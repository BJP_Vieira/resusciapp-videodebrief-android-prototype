package com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.mvp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.laerdal.rss.nobvi2.ressuscitationapp.bluetooth.BLEService
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxBus
import com.laerdal.rss.nobvi2.ressuscitationapp.bus.RxEvent
import com.laerdal.rss.nobvi2.ressuscitationapp.db.DBCrud
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Tag
import com.laerdal.rss.nobvi2.ressuscitationapp.model.TagClassifier
import com.laerdal.rss.nobvi2.ressuscitationapp.model.TrimVideo
import com.laerdal.rss.nobvi2.ressuscitationapp.model.Video
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.CameraRecordingUtils
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils.currentTime
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils.retrieveVideoDuration
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.record.ffmpegcallback.FFmpegBinaryCallback
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.camera.utils.TrimUtils
import com.otaliastudios.cameraview.CameraListener
import java.io.*
import com.laerdal.rss.nobvi2.ressuscitationapp.ui.debrief.DebriefingActivity
import com.laerdal.rss.nobvi2.ressuscitationapp.utils.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.realm.Realm
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import org.apache.commons.io.FilenameUtils
import javax.inject.Inject
import android.support.v4.os.HandlerCompat.postDelayed




class CameraPresenter @Inject
constructor(private val view: CameraActivityContract.View) : CameraActivityContract.Presenter {

    @Inject
    lateinit var context: Context
    @Inject
    lateinit var prefs: Prefs

    @Inject
    lateinit var fFmpeg: FFmpeg

    @Inject
    lateinit var realm: Realm
    private var videoFilePath: String? = null

    lateinit var recordFile: File
    private lateinit var videoModelObj: Video

    var camera: com.otaliastudios.cameraview.CameraView? = null
    var stopTime: Long = 0L

    private var notificationDataDisposable: Disposable? = null




    override fun registerCameraView(camera: com.otaliastudios.cameraview.CameraView?) {
        this.camera = camera
        this.camera?.addCameraListener(cameraListener)
    }


    override fun subscribe(){
        notificationDataDisposable = RxBus.listen(RxEvent.NotifyViewWithCompressiondata::class.java).observeOn(AndroidSchedulers.mainThread()).subscribe {
            view.sendCompressionDataToView(it.data)
        }
    }
    override fun unsubscribe() {
        RxBus.unSubscribe(notificationDataDisposable)
    }
    override fun isFileToDisplay(): Boolean {
        return !prefs.originalFilePath.isNullOrBlank()
    }

    override fun initiateFilePathValues() {
        videoModelObj = realm.where<Video>().equalTo("path", prefs.originalFilePath).findFirst()!!

    }

    override fun onRecordingClick() {
        videoFilePath = CameraRecordingUtils.getVideoFilePath(context)
        camera?.startCapturingVideo(File(videoFilePath))
        view.startRecording()
        context.startService(Intent(context, BLEService::class.java).setAction(BLEService.StartCollectingData))

        //Insert video in the DB, video is never null

        realm.executeTransaction {
            val maxId = realm.where(Video::class.java).max("id")
            val nextId = if (maxId == null) 1 else maxId.toLong() + 1
            videoModelObj = realm.createObject(nextId)
            videoModelObj.path = videoFilePath as String
            it.insertOrUpdate(videoModelObj)
        }
    }

    override fun onPauseClick() {
        camera?.stopCapturingVideo()
        view.pauseRecording()
    }


    override fun onThumbUpClick() {
        createTag(TagClassifier.Positive)
    }

    override fun onThumbDownClick() {
        createTag(TagClassifier.Negative)
    }

    private fun createTag(tagClassifier: TagClassifier) {
        view.playShutterSound()

        realm.beginTransaction()
        val tag = realm.createObject<Tag>()
        tag.classifier = tagClassifier.value
        tag.tagTimestamp = currentTime
        videoModelObj.tags.add(tag)
        realm.commitTransaction()
    }

    override fun onStopClick() {
        //On video taken will be called!
        camera?.stopCapturingVideo()
        view.stopRecording()
        context.startService(Intent(context, BLEService::class.java).setAction(BLEService.StopCollectingData))
        stopTime = currentTime
        view.showPreVideoChooser()
    }

    override fun processVideoWithMetadata() {
        processOriginalVideo()
    }

    private fun processOriginalVideo() {
        view.showLoader()
        recordFile = File(videoModelObj.path)
        view.proceedToOriginalVideoPreview(videoModelObj.path)
        view.hideLoader()

    }


    override fun processDebriefVideo() {
        view.showLoader()
        recordFile = File(prefs.originalFilePath)

        if (videoModelObj.trimmedVideos.isEmpty()) {
            view.hideLoader()
            view.proceedToOriginalVideoPreview(recordFile.absolutePath)
            return
        }

        TrimUtils.loadEditorBinary(fFmpeg)

        trimProcess(getTrimVideoListFromOriginalVideo().listIterator(), recordFile.absolutePath)
    }


    override fun goToVideoDebriefWithCPRCompression(debriefType: String) {
        val bundle = Bundle()
        bundle.putString(DebriefingActivity.videoSource, prefs.originalFilePath)
        context.startActivity(Intent(context, DebriefingActivity::class.java).putExtras(bundle).setAction(debriefType))
    }

    override fun goToTaggedVideoDebrief(debriefType: String, filePath: String) {
        val bundle = Bundle()
        bundle.putString(DebriefingActivity.videoSource, filePath)
        context.startActivity(Intent(context, DebriefingActivity::class.java).putExtras(bundle).setAction(debriefType))
    }


    override fun goToTrimedVideoDebrief(debriefType: String, filePath: String) {
        val bundle = Bundle()
        bundle.putString(DebriefingActivity.videoSource, filePath)
        context.startActivity(Intent(context, DebriefingActivity::class.java).putExtras(bundle).setAction(debriefType))
    }

    private var cameraListener = object : CameraListener() {
        override fun onVideoTaken(video: File) {
            recordFile = video
            prefs.originalFilePath = video.absolutePath

            processTagTimeIntoVideoRefTime(recordFile, stopTime)

        }

    }

    private fun getTrimVideoListFromOriginalVideo(): List<TrimVideo> {
        return videoModelObj.trimmedVideos
    }


    private fun trimProcess(trimVideoListIterator: ListIterator<TrimVideo>, filePath: String) {

        val trimVideo= trimVideoListIterator.next()

        TrimUtils.executeCuttingCommand(fFmpeg, filePath, trimVideo.path, trimVideo.tag?.tagTimestamp, ffmpegBinaryCallback = object : FFmpegBinaryCallback {
            override fun onSuccess(processedVideoPath: String) {
                if (trimVideoListIterator.hasNext()) {
                    trimProcess(trimVideoListIterator, filePath)
                } else {
                    view.hideLoader()
                    view.proceedToDebriefVideoPreview(filePath)
                }
            }

            override fun onError() {
                sendErrorToView("Trimming Error")
            }
        })
    }




    private fun processTagTimeIntoVideoRefTime(video: File, stopRecordingTime: Long) {
        val fileRootPath = FilenameUtils.getPath(video.absolutePath)
        val fileName = FilenameUtils.removeExtension(FilenameUtils.getName(video.absolutePath))
        val trimFile = fileRootPath + fileName




      videoModelObj.tags.iterator().forEach {
            realm.beginTransaction()
            val videoDuration = retrieveVideoDuration(context, video)
            val startTimeAtSystemClock = stopRecordingTime - videoDuration

            val nameBuilder = StringBuilder()
            nameBuilder.append(trimFile)
            nameBuilder.append("TrimNr_")
            nameBuilder.append("${it.tagTimestamp}.mp4")


            it.tagTimestamp = it.tagTimestamp - startTimeAtSystemClock

            val trimmedVideo = realm.createObject<TrimVideo>()
            trimmedVideo.tag = it
            trimmedVideo.path = nameBuilder.toString()


            videoModelObj.trimmedVideos.add(trimmedVideo)
        //    videoModelObj.tags.add(it)
            realm.commitTransaction()

        }
    }

    fun sendErrorToView(errorMessage: String) {
        view.hideLoader()
        view.showErrorMessage(errorMessage)
    }
}
